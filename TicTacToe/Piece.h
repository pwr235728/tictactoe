﻿#pragma once

#include <SFML/System.hpp>
enum player_type
{
	PLAYER_X,
	PLAYER_O,
	PLAYER_NONE
};

struct piece
{
	sf::Vector2i position;
	player_type type;

	piece(sf::Vector2i position, player_type type);
};

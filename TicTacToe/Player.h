﻿#pragma once

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "Piece.h"
#include "Board.h"


struct Board;
class TicTacToe;


class Player
{
public:
	virtual ~Player() = default;
	virtual sf::Vector2i turn(Board* board, TicTacToe* toe) = 0;
};

struct Move
{
	sf::Vector2i pos;
	int score;
};

class PlayerMinmax : public Player
{
public:
	PlayerMinmax(player_type type);
	virtual sf::Vector2i turn(Board* board, TicTacToe* toe) override;
	int score(Board* board_, int depth);
	int alphabeta(Board& board, int depth, player_type type, int a, int b);
	int minmax(Board board, int depth, player_type type);
	


private:
	player_type my_type_;
};

class PlayerHuman : public Player
{
public:
	PlayerHuman(sf::RenderWindow* window);
	~PlayerHuman() override;
	virtual sf::Vector2i turn(Board* board, TicTacToe* toe) override;

protected:

	sf::Vector2i process_events(Board* board, TicTacToe* toe);

private:
	sf::RenderWindow* window = nullptr;
};


﻿#include "TicTacToe.h"
#include <iostream>

float pointer_radius = 5.0f;
int board_size = 3; // diagonal

sf::CircleShape pointer(pointer_radius);


TicTacToe::TicTacToe(sf::VideoMode mode)
	: turn_(PLAYER_O), window_(mode, "Draughts")
{
	view_.setCenter(0.0f, 0.0f);
	view_.setSize(static_cast<float>(mode.width), static_cast<float>(mode.height));
	window_.setView(view_);
	
	std::cout << "Rozmiar planszyy n*n, podaj n: ";
	int size;
	std::cin >> size;

	std::cout << "\r\nPodaj wymagana dlugosc do wygranej: ";
	int win_length;
	std::cin >> win_length;

	std::cout << "\r\nKto ma byc \"O\"? bot/gracz [b/g]: ";
	char who_o;
	std::cin >> who_o;

	if (who_o == 'g')
		player_o_ = new PlayerHuman(&window_);
	else
		player_o_ = new PlayerMinmax(PLAYER_O);
	
	std::cout << "\r\nKto ma byc \"X\"? bot/gracz [b/g]: ";
	char who_x;
	std::cin >> who_x;
	if (who_x == 'g')
		player_x_ = new PlayerHuman(&window_);
	else
		player_x_ = new PlayerMinmax(PLAYER_X);

	board_.size = size;
	board_.win_length = win_length;
}

void TicTacToe::game_advance()
{
	std::cout << "tura: " << ((turn_ == PLAYER_O) ? "O" : "X") << "\n";

	sf::Vector2i pos;
	do
	{
		if (turn_ == PLAYER_O)
			pos = player_o_->turn(&board_, this);
		else
			pos = player_x_->turn(&board_, this);
	}
	while (!board_.is_empty(pos));
	piece piece(pos, turn_);
	board_.place_piece(piece);
	//int counter = board_->count_pieces(piece)
	if (board_.check_win(piece) != PLAYER_NONE || board_.free_places() == 0)
	{
		window_.close();
		if (board_.free_places() == 0)
		{
			std::cout << "Remis\n";
		}
		else
		{
			std::cout << "Wygral: " << ((turn_ == PLAYER_O) ? "O" : "X") << "\n";
		}
		//for (;;);
	}

	turn_ = turn_ == PLAYER_O ? PLAYER_X : PLAYER_O;
}

int TicTacToe::start()
{
	pointer.setOrigin(pointer_radius, pointer_radius);
	pointer.setFillColor(sf::Color::Cyan);

	while (window_.isOpen())
	{
		process_events();
		window_.clear(sf::Color::Black);
		window_.draw(board_);
		window_.draw(pointer);
		
		window_.display();
		game_advance();

	}

	return 0;
}

void TicTacToe::process_events()
{
	sf::Event event;
	while (window_.pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::Closed:
			close_app();
			break;

		case sf::Event::MouseButtonPressed:
			mouse_button_pressed(event.mouseButton);
			break;

		case sf::Event::MouseButtonReleased:
			mouse_button_released(event.mouseButton);
			break;

		case sf::Event::MouseMoved:
			mouse_moved(event.mouseMove);
			break;

		default:
			break;
		}
	}
}

void TicTacToe::close_app()
{
	window_.close();
}


void TicTacToe::mouse_button_pressed(const sf::Event::MouseButtonEvent& mouse_button)
{
	auto cords = window_.mapPixelToCoords({ mouse_button.x, mouse_button.y });
	pointer.setPosition(cords);
	pointer.setFillColor(sf::Color::Magenta);
	auto cords_board = board_.get_box(window_, { mouse_button.x, mouse_button.y });
}

void TicTacToe::mouse_button_released(const sf::Event::MouseButtonEvent& mouse_button)
{
	auto cords = window_.mapPixelToCoords({ mouse_button.x, mouse_button.y });
	pointer.setPosition(cords);
	pointer.setFillColor(sf::Color::Yellow);
}

void TicTacToe::mouse_moved(const sf::Event::MouseMoveEvent& mouse_move)
{
	auto cords = window_.mapPixelToCoords({ mouse_move.x, mouse_move.y });
	pointer.setPosition(cords);
	pointer.setFillColor(sf::Color::Cyan);
}


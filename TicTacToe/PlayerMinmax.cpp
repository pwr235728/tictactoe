#include "Board.h"
#include "Player.h"

Move choice;
int PlayerMinmax::score(Board* board_, int depth)
{
	auto winner = board_->get_winner();

	if (winner != PLAYER_NONE)
	{
		return ((winner == my_type_) ?
			std::pow(board_->size, 2) + 1 - depth :
			depth - (std::pow(board_->size, 2) + 1));
	}
	return 0;
}

static bool move_compare(Move a, Move b)
{
	return (a.score < b.score);
}


int PlayerMinmax::alphabeta(Board& board, int depth, player_type type,  int a, int b)
{
	if (board.get_winner() != PLAYER_NONE || board.free_places() == 0 || depth >= 7)
	{
		return score(&board, depth);
	}

	depth++;


	if(type != my_type_)
	{
		for (int y = 0; y < board.size; y++)
		{
			for (int x = 0; x < board.size; x++)
			{
				sf::Vector2i pos(x, y);
				if(board.is_empty(pos))
				{
					Board board_cpy = board;
					board_cpy.place_piece(piece{ pos, type });

					b = std::min(b, alphabeta(board_cpy, depth, type == PLAYER_O ? PLAYER_X : PLAYER_O, a, b));
					if( b <= a)
					{
						return b;
					}
				}
			}
		}
		return b;
	}
	else
	{
		for (int y = 0; y < board.size; y++)
		{
			for (int x = 0; x < board.size; x++)
			{
				sf::Vector2i pos(x, y);
				if (board.is_empty(pos))
				{
					Board board_cpy = board;
					board_cpy.place_piece(piece{ pos, type });

					a = std::max(a, alphabeta(board_cpy, depth, type == PLAYER_O ? PLAYER_X : PLAYER_O, a, b));
					if (b <= a)
					{
						return a;
					}
				}
			}
		}
		return a;
	}

}
int PlayerMinmax::minmax(Board board, int depth, player_type type)
{
	
	std::vector<Move> moves;
	moves.reserve(board.size*board.size);

	for (int y = 0; y< board.size; y++)
	{
		for (int x = 0; x<board.size; x++)
		{
			sf::Vector2i pos(x, y);
			if (board.is_empty(pos))
			{
				Board board_cpy = board;
				board_cpy.place_piece(piece{ pos, type });
				int score = alphabeta(board_cpy, depth, type == PLAYER_O ? PLAYER_X : PLAYER_O, std::numeric_limits<int>::min(), std::numeric_limits<int>::max());
				moves.push_back({ pos, score });
			}
		}
	}

	auto best = std::max_element(moves.begin(), moves.end(), move_compare);
	choice = *best;
	return best->score;
	
	
	/*
	if(board.get_winner() != PLAYER_NONE || board.free_places() == 0 || depth>8)
	{
		return score(&board, depth);
	}

	depth++;

	std::vector<Move> moves;

	for(int y=0; y< board.size; y++)
	{
		for(int x=0; x<board.size; x++)
		{
			sf::Vector2i pos(x, y);
			if(board.is_empty(pos))
			{
				Board board_cpy = board;
				player_type new_type = type == PLAYER_O ? PLAYER_X : PLAYER_O;
				board_cpy.place_piece(piece{ pos, type });
				int score = minmax(board_cpy, depth, new_type);
				moves.push_back({ pos, score });
			}
		}
	}

	// min or max 
	if(type == my_type_) // max
	{
		auto max = std::max_element(moves.begin(), moves.end(), move_compare);
		choice = *max;
		return max->score;
	}
	else // min
	{
		auto min = std::min_element(moves.begin(), moves.end(), move_compare);
		choice = *min;
		return min->score;
	}*/
}



PlayerMinmax::PlayerMinmax(player_type type)
	: my_type_(type)
{
}

sf::Vector2i PlayerMinmax::turn(Board* board, TicTacToe* toe)
{
	
	minmax(*board, 0, my_type_);
	return choice.pos;
}

﻿#include "Board.h"

float box_size = 50.0f;

//sf::Vector2i directions[8] = { { 1,0 },{ 1, 1 },{ 0, 1 },{ -1, 1 },{ -1, 0 },{ -1, -1 },{ 0, -1 },{ 1, -1 } };
sf::Vector2i directions[4] = { {1, 0}, {1, 1}, {0, 1}, {-1, 1} };

void Board::draw_board(sf::RenderTarget& target, sf::RenderStates states) const
{
	sf::RectangleShape line{ {box_size*size, 5} };
	line.setOrigin({ 0, -box_size + 2.5f });
	line.setFillColor(sf::Color::White);

	for (int i = 0; i < size - 1; i++)
	{
		line.setPosition({ 0, box_size*i });
		target.draw(line, states);
	}

	line = sf::RectangleShape{ {5, box_size*size} };
	line.setOrigin({ -box_size + 2.5f , 0});
	line.setFillColor(sf::Color::White);
	for (int i = 0; i<size - 1; i++)
	{
		line.setPosition({box_size*i ,0});
		target.draw(line, states);
	}
}
void Board::draw_pieces(sf::RenderTarget& target, sf::RenderStates states) const
{

	sf::RectangleShape line1 = sf::RectangleShape({ 2.f* box_size / 3.0f, 2 });
	sf::RectangleShape line2 = sf::RectangleShape({2,  2.f* box_size / 3.0f});

	line1.setOrigin(box_size / 3, 1);
	line2.setOrigin(1, box_size / 3 );
	line1.setFillColor(sf::Color::White);
	line2.setFillColor(sf::Color::White);
	line1.setRotation(45);
	line2.setRotation(45);

	for (auto i : player_x)
	{

		auto position = i.position * 50 + (sf::Vector2i)sf::Vector2f{ box_size / 2, box_size/2};
		line1.setPosition(position.x, position.y);
		line2.setPosition(position.x, position.y);
		target.draw(line1, states);
		target.draw(line2, states);
	}

	sf::CircleShape piece_b = sf::CircleShape(box_size / 3.0f);
	piece_b.setOrigin(box_size / 3 - box_size / 2, box_size / 3 - box_size / 2);
	piece_b.setFillColor(sf::Color::Transparent);
	piece_b.setOutlineThickness(2);
	piece_b.setOutlineColor(sf::Color::White);

	for (auto i : player_o)
	{

		auto position = i.position * 50;
		piece_b.setPosition(position.x, position.y);
		target.draw(piece_b, states);		
	}
}

void Board::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	const auto old_view = target.getView();
	const sf::View view = create_view();
	target.setView(view);

	draw_board(target, states);
	draw_pieces(target, states);

	target.setView(old_view);
}

sf::View Board::create_view() const
{
	sf::Vector2f view_size = { box_size*size, box_size*size };
	sf::Vector2f view_centre = view_size / 2.0f;
	sf::View view = sf::View(view_centre, view_size);
	return view;
}

sf::Vector2i Board::get_box(const sf::RenderWindow& wind, sf::Vector2i pixels) const
{
	auto view = create_view();
	auto cords = wind.mapPixelToCoords(pixels, view);
	return { static_cast<int>(cords.x / box_size), static_cast<int>(cords.y / box_size) };
}

bool Board::is_empty(const sf::Vector2i& position)
{
	for (auto& i : player_x)
	{
		if (i.position == position) return false;
	}
	for (auto& i : player_o)
	{
		if (i.position == position) return false;
	}

	return true;
}

bool Board::place_piece(piece piece)
{
	if (!is_empty(piece.position))
		return false;;

	if (piece.type == PLAYER_X)
		player_x.push_back(piece);
	else
		player_o.push_back(piece);

	last_piece = piece;

	return true;
}

bool Board::get_piece(sf::Vector2i position, piece** piece)
{
	if (position.x < 0 || position.x >= size || position.y < 0 || position.y >= size)
		return false;

	for (auto& i : player_x)
	{
		if (i.position == position)
		{
			*piece = &i;
			return true;
		}
	}
	for (auto& i : player_o)
	{
		if (i.position == position)
		{
			*piece = &i;
			return true;
		}
	}
	return false;
}

player_type Board::check_win(piece last_placed)
{
	std::vector<piece>* pieces = nullptr;
	if (last_placed.type == PLAYER_X)
		pieces = &player_x;
	else
		pieces = &player_o;

	for (const auto direction : directions)
	{
		piece* piece;
		sf::Vector2i position = last_placed.position;
		while(get_piece(position+direction, &piece) && piece->type == last_placed.type)
		{
			position += direction;
		}
		sf::Vector2i direction_op = { direction.x * -1, direction.y * -1 };
		int counter = 0;		
		for (int i = 0; i < win_length; i++)
		{
			if (get_piece(position, &piece) && piece->type == last_placed.type)
			{
				if (++counter == win_length)
					return last_placed.type;
			}
			else
			{
				break;
			}
			position += direction_op;
		}
	}

	return PLAYER_NONE;
}

player_type Board::get_winner()
{
	return check_win(last_piece);
}

int Board::free_places() const
{
	return size * size - player_o.size() - player_x.size();
}

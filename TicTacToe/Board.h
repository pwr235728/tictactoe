﻿#pragma once

#include <vector>

#include <SFML/Graphics.hpp>

#include "Piece.h"

struct Board : sf::Drawable
{

	int size;
	int win_length;

	std::vector<piece> player_x;
	std::vector<piece> player_o;

	sf::Vector2i get_box(const sf::RenderWindow& wind, sf::Vector2i pixels) const;
	bool is_empty(const sf::Vector2i& position);
	bool place_piece(piece piece);
	bool get_piece(sf::Vector2i position, piece** piece);

	player_type check_win(piece last_placed);
	player_type get_winner();
	int free_places() const;

protected:
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	sf::View create_view() const;

	piece last_piece{{0, 0 }, PLAYER_NONE};

private:
	void draw_board(sf::RenderTarget& target, sf::RenderStates states) const;
	void draw_pieces(sf::RenderTarget& target, sf::RenderStates states) const;
};

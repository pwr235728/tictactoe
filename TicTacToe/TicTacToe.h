﻿#pragma once

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <memory>

#include "Board.h"
#include "Player.h"

class TicTacToe
{
public:
	TicTacToe(sf::VideoMode mode = { 800, 600 });

	void game_advance();


	int start();
	void process_events();
private:

	Board board_;
	Player* player_x_;
	Player* player_o_;

	player_type turn_;

	sf::RenderWindow window_;
	sf::View view_;

	

	// events
	void close_app();
	void mouse_button_pressed(const sf::Event::MouseButtonEvent& mouse_button);
	void mouse_button_released(const sf::Event::MouseButtonEvent& mouse_button);
	void mouse_moved(const sf::Event::MouseMoveEvent& mouse_move);
};

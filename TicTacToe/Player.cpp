﻿#include "Player.h"
#include "Board.h"
#include "TicTacToe.h"

PlayerHuman::PlayerHuman(sf::RenderWindow* window): window(window)
{
}

PlayerHuman::~PlayerHuman()
{
}

sf::Vector2i PlayerHuman::turn(Board* board, TicTacToe* toe)
{
	return process_events(board, toe);
}

sf::Vector2i PlayerHuman::process_events(Board* board, TicTacToe* toe)
{
	while (!window->hasFocus() ||  !sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)) { toe->process_events(); }

	auto pos = sf::Mouse::getPosition(*window);
	auto cords_board = board->get_box(*window, pos);

	while (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)) { toe->process_events(); }

	return cords_board;
}

